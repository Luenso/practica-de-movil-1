#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_solorzanomedina_pm_facci_primeraapp4b_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
